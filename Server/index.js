import express from 'express'
import dbConnect from './utils/dbConnection.js'
import dotenv from 'dotenv'
import router from './Routes/AuthRoute.js'
import busRouter from './Routes/BusRoutes.js'
import routesRouter from './Routes/routeRouter.js'
import cookies from 'cookie-parser'
import bookingRouter from './Routes/BookingRoutes.js'
import cors from 'cors'
import helmet from 'helmet'
dotenv.config()

const app = express()

// used helmet to defend against basic xss
app.use(helmet())
app.use(cookies())
app.use(express.json())
// allows cross orgin from reactClient
app.use(
  cors({

    origin: 'http://localhost:5173'
  })
)

await dbConnect()

app.use(function (err, req, res, next) {
  console.log(err)
  return res.status(err?.status || 500).json({
    error: 'qwerty' || 'Something went wrong!'
  })
})

app.get('/', async (req, res) => {
  res.json('Hello World!')
})

app.use('/bus', busRouter)
app.use('/auth', router)
app.use('/api/', bookingRouter)
app.use('/route/', routesRouter)
app.use('*', (req, res) => {
  res.status(404).send('404 not found')
})

app.listen(8080, () => {
  console.log('listeng to port ')
})
