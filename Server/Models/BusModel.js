import mongoose from 'mongoose'
const Schema = mongoose.Schema

const Bus = new mongoose.Schema({
  operatorName: {
    type: String,
    required: true,
    trim: true
  },
  operatorId: {
    type: Schema.Types.ObjectId,
    ref: 'route',
    required: true
  },
  busType: {
    type: String,
    required: true,
    enum: ['ac', 'delux', 'normal', 'suspenseac', 'suspensedelux']
  },
  arrivalTime: {
    type: String,
    required: true
  },
  rating: {
    type: [Number],
    required: true
  },
  totalSeats: {
    type: Number,
    required: false
  },
  routeId: {
    type: Schema.Types.ObjectId,
    ref: 'route',
    required: true
  },
  liveTracking: {
    type: Boolean,
    required: true
  },
  reschedulable: {
    type: Boolean,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  facilities: [String]
})

export const busModel = mongoose.models.Bus || mongoose.model('bus', Bus)
