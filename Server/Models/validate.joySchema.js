import Joi from 'joi'
import joiPh from 'joi-phone-number'
const phoneNumberType = Joi.extend(joiPh)
export const userSchema = {
  email: Joi.string()
    .email({
      minDomainSegments: 2,
      tlds: { allow: ['com', 'net', 'co', 'in'] }
    })
    .required(),
  firstName: Joi.string().alphanum().min(5).max(30),
  lastName: Joi.string().alphanum().min(1).max(30),
  password: Joi.string().pattern(/^[a-zA-Z0-9]{3,30}$/),
  phoneNumber: phoneNumberType.string().phoneNumber({ defaultCountry: 'IN' }),
  address: Joi.string(),
  operatorName: Joi.string()
}

const stop = Joi.object({
  city: Joi.string(),
  latitude: Joi.number(),
  longtitude: Joi.number(),
  time: Joi.string().pattern(/([0-1]?[0-9]|2[0-3]):[0-5][0-9]/)
})

export const routeSchema = {
  routeCode: Joi.string(),
  source: stop,
  destination: stop,
  stops: Joi.array().items(stop),
  duration: Joi.number()
}

const passenger = Joi.object({
  name: Joi.string(),
  gender: Joi.string(),
  age: Joi.number().max(115).min(4)
})

export const BookingSchema = {
  customerId: Joi.string(),
  busId: Joi.string(),
  passengerDetails: Joi.array().items(passenger),
  email: Joi.string().email({
    minDomainSegments: 2,
    tlds: { allow: ['com', 'net', 'co', 'in'] }
  }),
  phoneNumber: phoneNumberType.string().phoneNumber({ defaultCountry: 'IN' }),
  fare: Joi.number(),
  status: Joi.string().valid('yet', 'cancelled', 'completed'),
  seats: Joi.array().items(Joi.number()),
  arrivalDetails: Joi.object({ city: Joi.string() }),
  departureDetails: Joi.object({ city: Joi.string() }),
  isInsurance: Joi.boolean(),
  pnr: Joi.string()
}

export const busSchema = {
  operatorId: Joi.string(),
  operatorName: Joi.string(),
  busType: Joi.string().valid('ac', '', 'delux', 'suspenseac', 'suspencedelux'),
  arrivalTime: Joi.string().pattern(/([0-1]?[0-9]|2[0-3]):[0-5][0-9]/),
  rating: Joi.array().items(Joi.number()),
  totalSeats: Joi.number(),
  routeCode: Joi.string(),
  liveTracking: Joi.boolean(),
  resheduleable: Joi.boolean().required(),
  price: Joi.number(),
  facilities: Joi.array().items(Joi.string())
}
