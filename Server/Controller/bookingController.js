// /book
// /cancel
// /view_bookings
import {
  viewMyBookings,
  updateBookingStatus,
  newBooking,
  viewAllBookings,
  viewBooking
} from '../services/booking.service.js'

export async function viewbookings (req, res) {
  const { busId, date } = req.body
  const bookings = viewAllBookings(busId)
  const bookingDetails = []
  for (const x of bookings) {
    if (x.departureDetails.date === date) bookingDetails.push(...x.seats)
  }
  bookingDetails.length > 0
    ? res.status(200).json(bookingDetails)
    : res.status(400).json('no bookings found')
}

export async function mybookings (req, res) {
  const booking = await viewMyBookings(req.user.email)
  booking.length > 0
    ? res.status(200).json(booking)
    : res.status(400).json('no bookings found')
}

export async function cancelBooking (req, res) {
  const booking = await updateBookingStatus(req.params.id, 'cancelled')
  booking.length > 0
    ? res.status(200).json(booking)
    : res.status(400).json('no bookings found')
}

export async function viewTicket (req, res) {
  const booking = await viewBooking(req.user.email, req.params.id)
  res.status(200).json(booking)
}

export async function bookTicket (req, res) {
  const {
    busId,
    passengerDetails,
    email,
    phoneNumber,
    fare,
    status,
    bookingDate,
    seats,
    departureDetails,
    arrivalDetails,
    isInsurance
  } = req.body
  // TODO: PNR GENREATION LOGIC
  const pnr = Math.random() * 100
  const booking = await newBooking(req.user.email, {
    busId,
    passengerDetails,
    email,
    phoneNumber,
    fare,
    status,
    bookingDate,
    seats,
    departureDetails,
    arrivalDetails,
    isInsurance,
    pnr
  })
  res.status(200).json(booking)
}
