import {
  createNewUser,
  verifyUser,
  newAcessToken,
  deleteToken,
  updateUser
} from '../services/user.services.js'

export async function registerUser (req, res) {
  const {
    firstName,
    lastName,
    email,
    password,
    phoneNumber,
    address,
    operatorName
  } = req.body
  const { accessToken, refreshToken } = await createNewUser({
    firstName,
    lastName,
    email,
    password,
    phoneNumber,
    address,
    operatorName
  })
  res.cookie('rftoken', {
    httpOnly: true,
    sameSite: 'None',
    secure: true,
    maxAge: 24 * 60 * 60 * 1000,
    value: refreshToken
  })
  return res.json({ accessToken, refreshToken })
}

export async function loginUser (req, res) {
  const { email, password } = req.body
  const { accessToken, refreshToken } = await verifyUser(email, password)
  res.cookie('rftoken', {
    httpOnly: true,
    sameSite: 'None',
    secure: true,
    maxAge: 24 * 60 * 60 * 1000,
    value: refreshToken
  })
  return res.json({ accessToken })
}

export async function refreshToken (req, res) {
  if (!req.cookies.rftoken) { return res.status(401).json({ message: 'Unauthorized' }) }
  const accessToken = await newAcessToken(req.cookies.rftoken)
  return res.json({ accessToken })
}

export async function logout (req, res) {
  if (!req.cookies.rftoken) { return res.status(401).json({ message: 'Unauthorized' }) }
  const refreshToken = req.cookies.rftoken
  await deleteToken(refreshToken)

  res.sendStatus(204)
}

export async function updateDetail (req, res) {
  await updateUser({ email: req.user.email }, req.body)
}
