// busRouter.get("/list");
// busRouter.post("/add_bus");
// busRouter.post("/newroute");
// busRouter.get("/routes");

import { addNewBus, findBusesByRoute } from '../services/bus.service.js'

export async function findBuses (req, res) {
  const { source, destination } = req.body
  const availableBuses = await findBusesByRoute(source, destination)
  res.status(200).json(availableBuses)
}

export async function addBus (req, res) {
  const {
    operatorName,
    busType,
    arrivalTime,
    totalSeats,
    routeCode,
    liveTracking,
    reschedulable,
    price
  } = req.body
  addNewBus({
    operatorName,
    busType,
    arrivalTime,
    totalSeats,
    routeCode,
    liveTracking,
    reschedulable,
    price
  }, req.user.email)
  res.status(200).json('sucess')
}
