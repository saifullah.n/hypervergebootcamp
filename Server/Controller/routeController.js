import { newRoute, availableRoutes } from '../services/route.service.js'

export async function addRoute (req, res) {
  const { routeCode, duration, source, destination, stops } = req.body
  await newRoute({ routeCode, duration, source, destination, stops })
  res.status(200).json('sucess')
}

export async function listRoutes (req, res) {
  const routesList = await availableRoutes()
  res.status(200).json(routesList)
}
