import { MongoMemoryServer } from 'mongodb-memory-server'
import mongoose from 'mongoose'

export const connectToTestDb = async () => {
  const mongoServer = await MongoMemoryServer.create()
  await mongoose.connect(mongoServer.getUri())
}
export const disconnectFromTestDb = async () => {
  await mongoose.connection.dropDatabase()
  await mongoose.connection.close()
}
