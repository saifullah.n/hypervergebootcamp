/* eslint-disable no-unused-expressions */
/* eslint-disable no-undef */
import { newRoute, availableRoutes, editRoute } from '../services/route.service.js'
import { connectToTestDb, disconnectFromTestDb } from './testMongo.db.js'

beforeAll(async () => {
  await connectToTestDb()
})
afterAll(async () => {
  await disconnectFromTestDb()
})

describe('Routes-Services', () => {
  const RouteObject = {
    routeCode: '12A',
    duration: 12,
    source: {
      city: 'A',
      latitude: 12.30090,
      time: '10:30',
      longitude: 13230.8
    },
    destination: {
      city: 'D',
      latitude: 12.800,
      time: '16:30',
      longitude: 132.3309
    },
    stops: [
      {
        city: 'B',
        latitude: 12.300,
        time: '12:30',
        longitude: 1323309.0
      },
      {
        city: 'C',
        latitude: 12.300,
        longitude: 1323309.0,
        time: '13:30'
      }
    ]
  }
  describe('Create Route', () => {
    it('creating new Route', async () => {
      const addedRoute = await newRoute(RouteObject)
      //   routeId = addedRoute._id.toString()
      expect(Object.is(RouteObject, addedRoute))
    })
    it('Creating Exisisting Route ', async () => {
      await newRoute(RouteObject).catch(e => expect(`NotAcceptableError:
       11000 duplicate key error collection: test.routes index: routeCode already exists`))
    })
  })
  describe('Listing Routes', () => {
    it('..', async () => {
      const routeList = await availableRoutes()
      expect(routeList.length)
      expect(routeList[0]).toHaveProperty('_id')
      expect(routeList[0]).toHaveProperty('source')
      expect(routeList[0]).toHaveProperty('destination')
      expect(routeList[0]).toHaveProperty('stops')
      expect(routeList[0]).toHaveProperty('routeCode')
    })
  })

  describe('Editing Routes', () => {
    it('', async () => {
      const newRouteObject = RouteObject.duration = 12
      await editRoute(RouteObject.routeCode, newRouteObject)
    })
  })
})
