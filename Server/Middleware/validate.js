import Joi from 'joi'
import createError from 'http-errors'

function validateMiddleware (schema) {
  return async function (req, res, next) {
    try {
      const validated = await Joi.object(schema).validateAsync(req.body)
      req.body = validated
      next()
    } catch (err) {
      //* Pass err to next
      //! If validation error occurs call next with HTTP 422. Otherwise HTTP 500
      if (err.isJoi) {
        err.status = 422
        return next(err)
      }
      next(createError(500))
    }
  }
}
export default validateMiddleware
