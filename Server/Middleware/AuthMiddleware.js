import jwt from 'jsonwebtoken'
import createError from 'http-errors'

function authenticateToken (authHeader) {
  const token = authHeader && authHeader.split(' ')[1]
  if (token == null) createError(401, 'Unauthorized')
  try {
    const user = jwt.verify(token, process.env.ACCESS_SECRET)
    return user
  } catch {
    createError(403, 'Unathorized')
  }
}

export function authenticateOperator (req, res, next) {
  const user = authenticateToken(req.headers.authorization)
  req.user = user
  return user.operatorName ? next() : res.sendStatus(403)
}

export function authenticateUser (req, res, next) {
  const user = authenticateToken(req.headers.authorization)
  req.user = user
  next()
}
