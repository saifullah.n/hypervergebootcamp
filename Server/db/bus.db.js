import { busModel } from '../Models/BusModel.js'
import dbErrorHandler from '../utils/dbErrorHandler.js'

/**
@param {Object} param
@param {Object} payload
@returns {Object}
**/

export async function createBus (payload) {
  const busObject = await busModel.create(payload).catch(dbErrorHandler)
  return busObject
}

/**
@param {Object} param
@param {Object} payload
@returns {Array}
**/
export async function findBuses (param, payload, filter) {
  const booking = await busModel.find(param, payload, filter || {}).catch(dbErrorHandler)
  return booking
}
// /**
// @param {String} email
// @returns {Object}
// **/
// export async function findBusByEmail (email) {
//   const   booking } = await BusModel.find({ email })
//   return   booking
// }

export async function findBus (param) {
  const booking = await busModel.find(param).catch(dbErrorHandler)
  return booking
}
