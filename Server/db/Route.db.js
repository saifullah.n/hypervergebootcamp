import { routeModel } from '../Models/RouteModel.js'
import dbErrorHandler from '../utils/dbErrorHandler.js'

/**
@param {Object} param
@param {Object} payload
@returns {Object}
**/

export async function createRoute (payload) {
  const route = await routeModel.create(payload).catch(dbErrorHandler)
  return route
}

/**
@param {Object} param
@param {Object} payload
@returns {Object}
**/
export async function updateRoute (param, payload) {
  const updatedRoute = await routeModel.findOneAndUpdate(param, payload).catch(dbErrorHandler)
  return updatedRoute
}
/**
@param {Object} param
@param {Object} filter
@returns {Array}
**/
export async function findRoutes (param, filter) {
  const routesList = await routeModel.find(param, filter || {}).catch(dbErrorHandler)
  return routesList
}

/**
@param {String} email
@returns {Array}
**/
export async function findRouteByCode (routeCode, filter) {
  const route = await routeModel.findOne(routeCode, filter || {}).catch(dbErrorHandler)
  return route
}
