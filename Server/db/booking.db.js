import { bookings } from '../Models/BookingModel.js'
import dbErrorHandler from '../utils/dbErrorHandler.js'

/**
@param {Object} param
@param {Object} payload
@returns {Object}
**/

export async function createBookingObject (payload) {
  const booking = await bookings.create(payload).catch(dbErrorHandler)
  return booking
}

/**
@param {Object} param
@param {Object} payload
@returns {Object}
**/
export async function updateBookingObject (param, payload) {
  const booking = await bookings.findOneAndUpdate(param, payload).catch(dbErrorHandler)
  return booking
}

export async function findBooking (param, filter) {
  const booking = await bookings.find(param, filter || {}).catch(dbErrorHandler)
  return booking
}
