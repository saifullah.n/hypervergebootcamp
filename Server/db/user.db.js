import { UserModel } from '../Models/UserModel.js'
import dbErrorHandler from '../utils/dbErrorHandler.js'

export async function findUserByEmail (email, filter) {
  const userData = await UserModel.findOne({ email }, filter || {}).catch(dbErrorHandler)
  return userData
}

export async function updateUserbyEmail (email, payload) {
  const userData = await UserModel.updateOne({ email }, payload).catch(dbErrorHandler)
  return userData
}

export async function createUser (payload) {
  const userData = await UserModel.create(payload).catch(dbErrorHandler)
  return userData
}

export async function deleteUser (payload) {
  const userData = await UserModel.findOneAndDelete(payload).catch(dbErrorHandler)
  return userData
}
