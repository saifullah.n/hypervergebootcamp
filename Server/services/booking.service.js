import createError from 'http-errors'
import { findBooking, updateBookingObject, createBookingObject } from '../db/booking.db.js'
import { findUserByEmail } from '../db/user.db.js'
export async function viewMyBookings (email) {
  const userData = await findUserByEmail(email)
  const bookings = await findBooking({ customerId: userData._id })
  return bookings
}

export async function updateBookingStatus (id, status) {
  const bookings = await updateBookingObject({ _id: id }, { status })
  return bookings
}

export async function newBooking (email, BookingObject) {
  const userData = await findUserByEmail(email)
  const booking = await createBookingObject({
    ...BookingObject,
    customerId: userData._id
  })
  return booking
}

export async function viewAllBookings (busId) {
  const bookings = await findBooking({ busId })
  return bookings
}

export async function viewBooking (email, id) {
  const userData = await findUserByEmail(email)
  const booking = await findBooking({ customerId: userData._id, _id: id })
  return booking
}
