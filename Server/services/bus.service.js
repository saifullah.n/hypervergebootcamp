import createError from 'http-errors'
import { findRouteByCode, findRoutes } from '../db/Route.db.js'
import { createBus, findBuses } from '../db/bus.db.js'
import { findUserByEmail } from '../db/user.db.js'

export async function addNewBus (busObject, email) {
  const route = await findRouteByCode({ routeCode: busObject.routeCode })[0]
  const operator = await findUserByEmail(email, { token: 0, password: 0 })
  await createBus({
    ...busObject,
    operatorId: operator._id,
    routeId: route._id
  })
}

export async function findBusesByRoute (source, destination) {
  const routeCodes = await findRoutes({
    $or: [
      { 'source.city': { $eq: source } },
      { stops: { $elemMatch: { name: { $eq: source } } } }
    ],
    // eslint-disable-next-line no-dupe-keys
    $or: [
      { 'destination.city': { $eq: destination } },
      { stops: { $elemMatch: { name: { $eq: destination } } } }
    ]
  },
  { routeCode: 1 })
  const availableBuses = await findBuses({
    routeId: { $in: routeCodes.map((object) => object._id) }
  })

  return availableBuses
}
