import createError from 'http-errors'
import { createRoute, findRoutes, updateRoute } from '../db/Route.db.js'

export async function newRoute (routeData) {
  const route = await createRoute(routeData)
  if (!route) createError(500, 'Something went wrong')
}

export async function availableRoutes () {
  const routes = await findRoutes({})
  return routes
}

export async function editRoute (routeCode, updatedData) {
  await updateRoute({ routeCode }, updatedData)
}
