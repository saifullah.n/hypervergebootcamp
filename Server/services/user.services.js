import bcrypt from 'bcryptjs'
import jwt from 'jsonwebtoken'
import createError from 'http-errors'
import { createUser, findUserByEmail, updateUserbyEmail } from '../db/user.db.js'

function generateAccessToken (object) {
  return jwt.sign(object, process.env.ACCESS_SECRET, {
    expiresIn: '1d'
  })
}

function generateRefreshToken (email) {
  return jwt.sign({ _id: email }, process.env.REFRESH_SECRET, {
    expiresIn: '1d'
  })
}

export async function createNewUser (user) {
  const salt = await bcrypt.genSalt()
  const hashPassword = await bcrypt.hash(user.password, salt)
  const accessToken = user.operatorName
    ? generateAccessToken({ email: user.email, operatorName: user.operatorName })
    : generateAccessToken({ email: user.email })
  const refreshToken = generateRefreshToken({ email: user.email })
  await createUser({ ...user, password: hashPassword, token: refreshToken })
  return { accessToken, refreshToken }
}

export async function verifyUser (email, password) {
  const userData = await findUserByEmail(email)
  if (!userData) throw createError(403, 'invalid Credentials')
  const match = await bcrypt.compare(password, userData.password)

  if (!match) throw createError(403, 'invalid Credentials')
  const accessToken = userData.operatorName
    ? generateAccessToken({ email, operatorName: userData.operatorName })
    : generateAccessToken({ email })
  const refreshToken = generateRefreshToken(email)
  await updateUserbyEmail(email, { token: refreshToken })
  return { accessToken, refreshToken }
}

export async function newAcessToken (refreshToken) {
  let accessToken = ''
  jwt.verify(refreshToken, process.env.REFRESH_SECRET, async (err, decoded) => {
    if (err) {
      return createError(401, 'Unauthorized')
    } else {
      const user = await findUserByEmail(decoded.email)
      if (!user) return createError(401, 'Unauthorized')
      accessToken = generateAccessToken(decoded.email)
    }
  })

  return accessToken
}

export async function deleteToken (refreshToken) {
  jwt.verify(refreshToken, process.env.REFRESH_SECRET, async (_err, decoded) => {
    await updateUserbyEmail({ email: decoded.email }, { token: '' })
  })
}

export async function updateUser (email, payload) {
  await updateUserbyEmail(email, payload)
}
