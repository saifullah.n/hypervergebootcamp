import createError from 'http-errors'
export default function dbErrorHandler (error) {
  let message = ''
  let status = 400
  if (error.code) {
    switch (error.code) {
      case 11000:
      case 11001:
        message = uniqueMessage(error)
        status = 406
        break
      default:
    }
  } else {
    if (error.message.indexOf('Cast to ObjectId failed') !== -1) {
      message = 'No data found'
      status = 400
    }
    for (const errorName in error.errors) {
      if (error.errors[errorName].message) {
        message = error.errors[errorName].message
        status = 400
      }
    }
  }
  console.log('Error--> ', error)
  if (message.includes('Path')) {
    message = message.slice(6)
  }
  console.log('Message--> ', message)
  throw createError(status, message)
};

function uniqueMessage (error) {
  let output
  try {
    const fieldName = error.message.substring(
      error.message.lastIndexOf('.$') + 2,
      error.message.lastIndexOf('_1')
    )
    output =
              fieldName.charAt(0).toUpperCase() +
              fieldName.slice(1) +
              ' already exists'
  } catch (ex) {
    output = 'Unique field already exists'
  }
  return output
};
