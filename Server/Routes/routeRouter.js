import { Router } from 'express'
import { authenticateOperator } from '../Middleware/AuthMiddleware.js'
import {
  addRoute,
  listRoutes
} from '../Controller/routeController.js'
import validateMiddleware from '../Middleware/validate.js'
import { routeSchema } from '../Models/validate.joySchema.js'
import controllerErrors from '../utils/controllerErrorHandler.js'
const routesRouter = new Router()
routesRouter.use(authenticateOperator)

const addRouteSchema = Object.assign({}, routeSchema, {
  routeCode: routeSchema.routeCode.required(),
  duration: routeSchema.duration.required(),
  source: routeSchema.source.required(),
  destination: routeSchema.destination.required(),
  stops: routeSchema.stops.required()
})

routesRouter.post('/newroute', validateMiddleware(addRouteSchema), controllerErrors(addRoute))
routesRouter.get('/routes', controllerErrors(listRoutes))

export default routesRouter
