import { Router } from 'express'
import {
  registerUser,
  loginUser,
  refreshToken,
  logout,
  updateDetail
} from '../Controller/AuthController.js'
import { authenticateUser } from '../Middleware/AuthMiddleware.js'
import { userSchema } from '../Models/validate.joySchema.js'
import validateMiddleware from '../Middleware/validate.js'
import controllerErrors from '../utils/controllerErrorHandler.js'
// Initialization
const router = Router()

// Requests

const registerSchema = Object.assign({}, userSchema, {
  firstName: userSchema.firstName.required(),
  lastName: userSchema.lastName.required(),
  password: userSchema.password.required()
})
router.post('/register', validateMiddleware(registerSchema), controllerErrors(registerUser))

const loginSchema = Object.assign({}, userSchema, {
  password: userSchema.password.required()
})
router.post('/login', validateMiddleware(loginSchema), controllerErrors(loginUser))

router.use(validateMiddleware(userSchema))
router.post('/refresh', controllerErrors(refreshToken))
router.delete('/logout', controllerErrors(logout))

router.use(authenticateUser)
router.post('/update', controllerErrors(updateDetail))
export default router
