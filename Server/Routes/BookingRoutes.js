import { Router } from 'express'
import {
  bookTicket,
  viewbookings,
  cancelBooking,
  viewTicket,
  mybookings
} from '../Controller/bookingController.js'
import { authenticateUser } from '../Middleware/AuthMiddleware.js'
import { BookingSchema } from '../Models/validate.joySchema.js'
import validateMiddleware from '../Middleware/validate.js'
import controllerErrors from '../utils/controllerErrorHandler.js'
import Joi from 'joi'
const bookingRouter = new Router()

const bookSchema = Object.assign({}, BookingSchema, {
  busId: BookingSchema.busId.required(),
  passengerDetails: BookingSchema.passengerDetails.required(),
  phoneNumber: BookingSchema.phoneNumber.required(),
  email: BookingSchema.email.required(),
  fare: BookingSchema.fare.required(),
  seats: BookingSchema.seats.required(),
  bookingDate: Joi.string(),
  departureDetails: BookingSchema.departureDetails.required(),
  arrivalDetails: BookingSchema.arrivalDetails.required()
})

bookingRouter.use(authenticateUser)
bookingRouter.post('/book', validateMiddleware(bookSchema), controllerErrors(bookTicket))
bookingRouter.post('/cancel', controllerErrors(cancelBooking))
bookingRouter.post('/view_bookings', controllerErrors(viewbookings))
bookingRouter.get('/my_booking', controllerErrors(mybookings))
bookingRouter.post('/view_ticket:id', controllerErrors(viewTicket))

export default bookingRouter
