import { Router } from 'express'
import { authenticateOperator, authenticateUser } from '../Middleware/AuthMiddleware.js'
import {
  addBus,
  findBuses
} from '../Controller/BusController.js'
import validateMiddleware from '../Middleware/validate.js'
import { busSchema, routeSchema } from '../Models/validate.joySchema.js'
import Joi from 'joi'
import controllerErrors from '../utils/controllerErrorHandler.js'
const busRouter = new Router()
busRouter.post(
  '/buses',
  authenticateUser,
  validateMiddleware({
    source: Joi.string().required(),
    destination: Joi.string().required()
  }),
  controllerErrors(findBuses)
)

busRouter.post('/addbus', authenticateOperator, validateMiddleware(busSchema), controllerErrors(addBus))

export default busRouter
