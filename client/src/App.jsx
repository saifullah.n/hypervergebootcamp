import './App.css'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import ProtectedRoute from './components/ProtectedRoute'
import NavBar from './components/Navbar'
import Login from './pages/Login'
import Register from './pages/Register'
import Booking from './pages/Booking'
import BookTicket from './pages/BookTicket'

function App() {
  return (
    <>
    {/* <NavBar /> */}
<BrowserRouter>
  <Routes>
    <Route path="/" element={<h1>Home</h1>} />
    <Route
      path="/login"
      element={
        <Login />
      }
    />
    <Route
      path="/register"
      element={
        <Register />
      }
    />
    <Route
      path="/dashboard"
      element={
      <ProtectedRoute>
          <h1>Product</h1>
      </ProtectedRoute>}
    />
    <Route
      path="/booking"
      element={
      // <ProtectedRoute>
          <Booking/>
      // </ProtectedRoute>
    }
    />
    <Route
      path="/book_ticket"
      element={
      // <ProtectedRoute>
          <BookTicket/>
      // </ProtectedRoute>
    }
    />
  </Routes>
</BrowserRouter>
    </>
  )
}

export default App