import PropTypes from 'prop-types';
import { Navigate } from 'react-router-dom'
import { useSelector } from 'react-redux/es/hooks/useSelector';
export default function ProtectedRoute({ children }) {
  const user =  useSelector((state)=>state.user)
  if (!user) {
    return <Navigate to="/" replace />
  }
  return children
}
ProtectedRoute.propTypes = {
    children: PropTypes.node.isRequired
  }
  