import React from 'react'

function Navbar() {
  return (<>
  <div className='flex flex-col justify-evenly'>

  <div className="w-40 h-14 justify-start items-center gap-3.5 inline-flex">
  <img className="w-14 h-14 rounded-2xl" src="https://via.placeholder.com/55x55" />
  <div className="flex-col justify-start items-start inline-flex">
    <div className="text-indigo-950 text-base font-bold">User Name</div>
  </div>
</div>
 <div className='flex flex-col justify-between h-screen '>
<div className="w-64 h-1/2 px-4 flex-col justify-start items-start gap-2 inline-flex">
  <div className="self-stretch justify-start items-start inline-flex">
    <div className="w-1.5 self-stretch rounded-3xl" />
    <div className="grow shrink basis-0 h-12 pl-4 pr-2 py-2 justify-start items-center gap-2.5 flex">
      <div className="w-8 h-8 relative" />
      <div className="text-slate-500 text-sm font-normal">Profile</div>
    </div>
  </div>
  <div className="self-stretch justify-start items-start inline-flex">
    <div className="w-1.5 self-stretch rounded-3xl" />
    <div className="grow shrink basis-0 h-12 pl-4 pr-2 py-2 justify-start items-center gap-2.5 flex">
      <div className="w-8 h-8 relative" />
      <div className="text-slate-500 text-sm font-normal">BLA BLA</div>
    </div>
  </div>
  <div className="self-stretch justify-start items-start inline-flex">
    <div className="w-1.5 self-stretch rounded-3xl" />
    <div className="grow shrink basis-0 h-12 pl-4 pr-2 py-2 justify-start items-center gap-2.5 flex">
      <div className="w-8 h-8 relative" />
      <div className="text-slate-500 text-sm font-normal">bLA BLA</div>
    </div>
  </div>
  <div className="self-stretch justify-start items-start inline-flex">
    {/* <div className="w-1.5 self-stretch bg-red-500 rounded-3xl" /> */}
    <div className="grow shrink basis-0 h-12 pl-4 pr-2 py-2 justify-start items-center gap-2.5 flex">
      <div className="w-8 h-8 relative" />
      <div className="text-indigo-950 text-sm font-normal">Book Tickets</div>
    </div>
  </div>
  <div className="self-stretch justify-start items-start inline-flex">
    <div className="w-1.5 self-stretch rounded-3xl" />
    <div className="grow shrink basis-0 h-12 pl-4 pr-2 py-2 justify-start items-center gap-2.5 flex">
      <div className="w-8 h-8 relative" />
      <div className="text-slate-500 text-sm font-normal">View Bookings</div>
    </div>
  </div>
</div>
<div className="w-64 h-12 justify-start items-start inline-flex">
  <div className="w-1.5 self-stretch rounded-3xl" />
  <div className="grow shrink basis-0 h-12 pl-4 pr-2 py-2 justify-start items-center gap-2.5 flex">
    <div className="w-8 h-8 relative" />
    <div className="text-slate-500 text-sm font-normal">Log out</div>
  </div>
  </div>
</div>
  </div>

  </>)
   
}

export default Navbar