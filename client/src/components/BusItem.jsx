import { motion } from 'framer-motion'
import PropTypes from 'prop-types';
import { useNavigate } from 'react-router-dom';
import { getBookings, updateBooking} from '../redux/BookingSlice'
import { useDispatch } from 'react-redux';
function BusItem({operatorName,busType,arrivalTime,totalSeats,price , id, date }) {
  const dispacther = useDispatch()
  const history = useNavigate()
  async function openBooking(id){
    const response = await getBookings({id,date})
    if('bookingDetails' in response) dispacther(updateBooking({id,date,seats:response.bookingDetails}))
    history(`/book_ticket?id=${id}&date=${date}`)
  
}
  return (
<>
<motion.div  onClick={()=>openBooking(id)}  whileHover={{ scale: 0.99 }}
 className="flex flex-row justify-around w-8/12  bg-white shadow h-11 py-2 ">
<motion.div className='w-8/12 text-center'>
  {operatorName}
</motion.div>

<motion.div className='w-8/12 text-center'>
{busType}
</motion.div>
  
<motion.div className='w-8/12 text-center'>
  {arrivalTime}
</motion.div>
<motion.div className='w-8/12 text-center'> 
    {totalSeats}
</motion.div>
<motion.div className='w-8/12 text-center'> 
    {price}
</motion.div>
</motion.div>
</>
)
}

export default BusItem

BusItem.propTypes = {
  operatorName: PropTypes.string.isRequired,
  busType:PropTypes.string.isRequired,
  arrivalTime:PropTypes.string.isRequired,
  totalSeats:PropTypes.number.isRequired,
  price:  PropTypes.number.isRequired,
  id: PropTypes.string.isRequired

}
