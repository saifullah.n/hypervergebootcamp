import React, { useState } from 'react';
import { useSelector } from 'react-redux/es/hooks/useSelector';
import '../App.css';

const BookTicket = () => {
      const id = Object.fromEntries(new URLSearchParams(window.location.search).entries()).id;
      const buses = useSelector(state=>state.buses.availbleBuses )
      const bookings = useSelector(state =>state.bookings)
      const bookedSeats = bookings.seats
      const bus = buses.filter((bus)=> bus._id == id)[0]
      const totalRows = Math.floor(bus.totalSeats/3); // Change this to the total number of rows
      const seatsPerRow = 3; // Change this to the number of seats per row
      const [selectedSeats, setSelectedSeats] = useState([]);

  const isSeatBooked = (seatNumber) => {
    return bookedSeats.includes(seatNumber);
  };

  const handleSeatClick = (seatNumber) => {
    if (isSeatBooked(seatNumber)) {
      return; // Seat is already booked, do nothing
    }

    if (selectedSeats.includes(seatNumber)) {
      setSelectedSeats(selectedSeats.filter((seat) => seat !== seatNumber));
    } else {
      setSelectedSeats([...selectedSeats, seatNumber]);
    }
  };

  return (
    <div className="seat-picker-container">
      <h2>Seat Picker</h2>
      <div className="seat-picker">
        {Array.from({ length: totalRows }, (_, rowIndex) => (
          <div key={rowIndex} className="seat-row">
            {Array.from({ length: seatsPerRow }, (_, seatIndex) => {
              const seatNumber = rowIndex * seatsPerRow + seatIndex + 1;
              const isBooked = isSeatBooked(seatNumber);
              const isSelected = selectedSeats.includes(seatNumber);

              return (
                <div
                  key={seatNumber}
                  className={`seat ${isBooked ? 'booked' : ''} ${
                    isSelected ? 'selected' : ''
                  }`}
                  onClick={() => handleSeatClick(seatNumber)}
                >
                  {seatNumber}
                </div>
              );
            })}
          </div>
        ))}
      </div>
      <div>
        <h3>Selected Seats:</h3>
        <ul>
          {selectedSeats.map((seat) => (
            <li key={seat}>{seat}</li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default BookTicket;