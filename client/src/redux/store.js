import { configureStore} from '@reduxjs/toolkit'
import user from './UserStore.js'
import buses from './BusesSlice.js'
import bookings from './BookingSlice.js'

export default configureStore({
  reducer: {
   user,
   buses,
   bookings
  },
})