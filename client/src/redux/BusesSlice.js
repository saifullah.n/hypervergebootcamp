import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import { acessPrivateRoute } from '../api/acessPrivateRoute';

const initialState = {
source:'',
destination:'',
date:'',
availbleBuses:[]
};

export const getBuses = 
  createAsyncThunk('bus/buses', async (payload) => {
    try {
      console.log(payload)
      const response = await acessPrivateRoute.post('bus/buses',{...payload});
      return response.data;
    } catch (error) {
      throw error.response?.data.message;
    }
  });

export const BusesSlice = createSlice({
  name: 'buses',
  initialState,
  reducers: {
    clearSearch : function (state) {
        state.source = '',
        state.destination = '',
        state.availbleBuses = ''
      },
    updateSrcDest : function (state,action) {
        state.source =action.payload.source ,
        state.destination = action.payload.destination
      },
    
    swapSourceDestination(state){
        [ state.source , state.destination ] = [state.destination , state.source]
    },

    updateAvailableBuses(state,action){
        state.availbleBuses = action.payload.buses
    },
    selectBusById(state){
              console.log(state.availbleBuses);
     return  state.availbleBuses
    }

  },
  extraReducers(builder) {
    builder
      .addCase(getBuses.pending, (state) => {
      })
      .addCase(getBuses.fulfilled, (state) => {
        state.loading = false;
        state.error = false;
        // availbleBuses = 
      })
      .addCase(getBuses.rejected, (state, action) => {
        state.loading = false;
        state.error = true;
        state.err = action.error;
      });
  },
});
export const { swapSourceDestination , updateAvailableBuses , clearSearch  ,selectBusById , updateSrcDest} = BusesSlice.actions;
export default BusesSlice.reducer;