import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

const initialState = {
  name: '',
  email: '',
  emailVerified: false,
  loading: false,
  error: false,
  err: null,
};

export const getBuses =
  createAsyncThunk('/buses', async () => {
    try {
      const response = await axios.post('user/buses');
      return response.data;
    } catch (error) {
      throw error.response?.data.message;
    }
  });

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    clearUserInfo : function (state) {
          state.email = '';
          state.emailVerified = false;
          state.err = null;
          state.error = false;
          state.loading = false;
          state.name = '';
      },
  },
  extraReducers(builder) {
    builder
      .addCase(getUserInfo.pending, (state) => {
        state.loading = true;
        state.error = false;
        state.err = null;
        state.name = '';
        state.email = '';
        state.emailVerified = false;
      })
      .addCase(getUserInfo.fulfilled, (state, action) => {
        state.loading = false;
        state.error = false;
        state.err = null;
        state.name = action.payload.name;
        state.email = action.payload.email;
        state.emailVerified = action.payload.emailVerified;
      })
      .addCase(getUserInfo.rejected, (state, action) => {
        state.name = '';
        state.email = '';
        state.emailVerified = false;
        state.loading = false;
        state.error = true;
        state.err = action.error;
      });
  },
});
export const { clearUserInfo } = userSlice.actions;
export default userSlice.reducer;