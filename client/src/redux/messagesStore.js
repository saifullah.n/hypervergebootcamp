import { createSlice } from "@reduxjs/toolkit"

const initialState = {
  message:[{label:'', level:''}]
}

// Then, handle actions in your reducers:
const messageSlice = createSlice({
  name: 'messageSlice',
  initialState,
  reducers: {
    pushNewMessage(state, action){
      state.message.push(action.payload)
    },

    clearMessages(state){
      state.message=[]
    },

    popAMessage(state,){
      state.message.pop()
    }

  },
})

export default messageSlice.reducer