import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import { acessPrivateRoute } from '../api/acessPrivateRoute';

const initialState = {
    id:'',
    date:'',
    seats:[],
    seletedSeats:[]
};

export const getBookings = 
  createAsyncThunk('bus/view_bookings', async (payload) => {
    try {
      console.log(payload)
      const response = await acessPrivateRoute.post('bus/view_bookings',{...payload});
      return response.data;
    } catch (error) {
      throw error.response?.data.message;
    }
  });

export const BookingSlice = createSlice({
  name: 'booking',
  initialState,
  reducers: {
    updateBooking:function(state,action){
      state.id = action.payload.id
      state.date = action.payload.date
      state.seats = action.payload.seats
    },
    selectSeats:function(state , action){
        const seats = state.seletedSeats
        const index = seats.indexOf(action.payload.seletedSeat)
        if (index > -1){
        seats.splice(index, 1);
        state.selectSeats = seats 
      }
      else{ 
        seats.push(action.payload.seletedSeat)
        state.seletedSeats = seats
      }
    }
  },
  extraReducers(builder) {
    builder
      .addCase(getBookings.pending, (state) => {
      })
      .addCase(getBookings.fulfilled, (state) => {
        state.loading = false;
        state.error = false;
        // availbleBuses = 
      })
      .addCase(getBookings.rejected, (state, action) => {
        state.loading = false;
        state.error = true;
        state.err = action.error;
      });
  },
});
export const {updateBooking , selectSeats } = BookingSlice.actions;
export default BookingSlice.reducer 