import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import axios from 'axios'
// First, create the thunk
export const loginUser = createAsyncThunk(
  '/auth/login',
async (payload) => {
    try {
      const response = await axios.post("http://localhost:8080/auth/login", {
        ... payload
        })
        return response.data;
    } catch (error) {
       const err = error;
            err.message = err.response?.data.message;
            throw err;
            }        
  }
)
export const registerUser = createAsyncThunk(
  '/auth/register',
  async (payload) => {
    try {
      const response = await axios.post("http://localhost:8080/auth/register", {
        ... payload
        })
        return response.data;
    } catch (error) {
       const err = error;
            err.message = err.response?.data.message;
            throw err;
            }        
  }
)

// const updateUser = createAsyncThunk(
//   '/auth/login',
//   async (token, payload) => {
//     const response = await acessPrivateRoute(token,'auth/update').post(payload)
//     return response.data
//   }
// )

const initialState = {
  email:'',
  loading:'',
  name:'',
  token:'',
  refreshToken:''
}

// Then, handle actions in your reducers:
const usersSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    fetchUserSession:function (state) {
    if(!sessionStorage.getItem('user')) throw('no valid session')
    const user = JSON.parse(sessionStorage.getItem('user'))
    user? state =structuredClone(user) : state = initialState
    },

    setUserSession:function (state) {
    sessionStorage.setItem('user',JSON.stringify(state))
    },

    updateUser:function(state , action){
      state.email = action.payload.email
      state.token = action.payload.token
      state.name = action.payload.name
      state.refreshToken = action.payload.refreshToken
      state.loading = 'idle'
    },

    updateToken:function (state , action) {
     state.token = action.payload
    }
  },
  extraReducers: (builder) => {
    // Add reducers for additional action types here, and handle loading state as needed
    builder.addCase(loginUser.pending, (state) => {
      state.loading="loading"
    })
    builder.addCase(loginUser.rejected, (state) => {
      state.loading="error"
    })
    builder.addCase(loginUser.fulfilled, (state, action) => {
      state =Object.assign(state ,action.payload)
      state.loading="sucess"
    })
  },
})
const actions = usersSlice.actions
export const {updateToken , updateUser , fetchUserSession ,setUserSession} = actions
export default usersSlice.reducer
// builder.addCase(updateUser.fulfilled, (state, action) => {
//   state = action.payload,
//   state.loading="sucess"
//   builder.addCase(updateUser.rejected, (state, action) => {
//     state.loading="error"
//   })
// })