import React, { useState } from "react";
import { useSelector } from "react-redux/es/hooks/useSelector";
import "../App.css";
import { acessPrivateRoute } from "../api/acessPrivateRoute";
import { useNavigate } from "react-router-dom";

function BookTicket() {
  const params = Object.fromEntries(
    new URLSearchParams(window.location.search).entries()
  );
  const id = params.id
  const date = params.date
  const buses = useSelector((state) => state.buses.availbleBuses);
  const routeData = useSelector(state => state.buses)
  const bookings = useSelector((state) => state.bookings);
  const bookedSeats = bookings.seats;
  const bus = buses.filter((bus) => bus._id == id)[0];
  const totalRows = Math.floor(bus.totalSeats/3); // Change this to the total number of rows
  const seatsPerRow = 3; // Change this to the number of seats per row
  const [selectedSeats, setSelectedSeats] = useState([]);
  const [passengerData, setPassengerData] = useState({});
  const [phoneNumber, setPhoneNumber] = useState('');
  const [email,setEmail] = useState('');
  const history = useNavigate()

  const isSeatBooked = (seatNumber) => {
    return bookedSeats.includes(seatNumber);
  };

  const handleSeatClick = (seatNumber) => {
    if (isSeatBooked(seatNumber)) {
      return; // Seat is already booked, do nothing
    }
    
    if (selectedSeats.includes(seatNumber)) {
      setSelectedSeats(selectedSeats.filter((seat) => seat !== seatNumber));
      setPassengerData((prevData) => {
        const newData = { ...prevData };
        delete newData[seatNumber];
        return newData;
      });
    } else {
      if (selectedSeats.length >= 3)
        return alert("only can book a max of 3 seats");
      setSelectedSeats([...selectedSeats, seatNumber]);
      setPassengerData((prevData) => ({ ...prevData, [seatNumber]: {} }));
    }
  };

  const handleInputChange = (seatNumber, field, value) => {
    setPassengerData((prevData) => ({
      ...prevData,
      [seatNumber]: { ...prevData[seatNumber], [field]: value },
    }));
  };
  const handleBooking = async () => {
    if (selectedSeats.length <= 0)
      return alert("atlest one seat must be booked");
   try {
    let response = await acessPrivateRoute.post("/api/book", {
      busId: id,
      passengerDetails:Object.values(passengerData),
      email,
      phoneNumber,
      fare: selectedSeats.length * bus.price,
      status:'yet',
      bookingDate :date,
      seats: selectedSeats,
      departureDetails:{city:routeData.source},
      arrivalDetails:{city:routeData.destination}
    });
    if(response)
    alert('booked Sucessfully')
    history('/')
    
   } catch (error) {
      alert('something went wrong')
      history('/booking')
   }
  };
  return (
    <div className="seat-picker-container w-screen">
    <h2 className="font-sans text-xl font-semibold tracking-tight text-indigo-950  p-3">Select Seats</h2>
    <div className="seat-picker w-50 mt-10  p-2 ">
      {Array.from({ length: totalRows }, (_, rowIndex) => (
        <div key={rowIndex} className="seat-row">
          {Array.from({ length: seatsPerRow }, (_, seatIndex) => {
            const seatNumber = rowIndex * seatsPerRow + seatIndex + 1;
            const isBooked = isSeatBooked(seatNumber);
            const isSelected = selectedSeats.includes(seatNumber);

            return (
              <div
                key={seatNumber}
                className={`seat ${isBooked ? 'booked' : ''} ${
                  isSelected ? 'selected' : ''
                }`}
                onClick={() => handleSeatClick(seatNumber)}
              >
                {seatNumber}
              </div>
            );
          })}
        </div>
      ))}
    </div>
    <div className="flex flex-col justify-around">
      <div>
      <h3 className="font-sLans text-xl font-semibold tracking-tight text-indigo-950  p-3">Booking Form</h3>
    {selectedSeats.length > 0 && (
      <div className="booking-form">
        {selectedSeats.map((seat) => (
          <div key={seat}>
            <p>Passenger  For Seat {seat}:</p>
            <input
              type="text"
              placeholder="Name"
              required
              value={passengerData[seat]?.name || ''}
              onChange={(e) => handleInputChange(seat, 'name', e.target.value)}
            />
            <select
                value={passengerData[seat]?.gender || ''}
                onChange={(e) => handleInputChange(seat, 'gender', e.target.value)}
              >
                <option value="">Select Gender</option>
                <option value="male">Male</option>
                <option value="female">Female</option>
            </select>
            <input
              type="number"
              required
              placeholder="Age"
              value={passengerData[seat]?.age || ''}
              onChange={(e) => handleInputChange(seat, 'age', e.target.value)}
            />
          </div>
        ))}
      </div>
    )} 
    </div>
    <div className="flex flex-col justify-evenly">
    <h4 className="font-sans text-xl font-semibold tracking-tight text-indigo-950  p-3">Other Details</h4>
      <input type="email" name="email" placeholder="email" onChange={(event)=>setEmail(event.target.value)} className="font-sans text-l font-semibold tracking-tight text-indigo-950  p-3" />
      <input type="number" name="pHno" placeholder="phNo" onChange={(event)=>setPhoneNumber(event.target.value)} className="font-sans text-l font-semibold tracking-tight text-indigo-950  p-3"/>
    <button className="rounded bg-blue-500 hover:bg-blue-700 py-2 px-4 text-white"onClick={handleBooking} > BOOK NOW</button>
    </div>

    </div>
  </div>
  );
}

export default BookTicket;
