import  { useState } from 'react'
import {MdSwapVerticalCircle ,MdOutlineSearch} from 'react-icons/md'
import { motion , AnimatePresence} from 'framer-motion';
import DatePicker from "react-horizontal-datepicker";
import  BusItem  from '../components/BusItem';
import { useDispatch, useSelector } from 'react-redux';
import { getBuses , updateAvailableBuses , updateSrcDest } from '../redux/BusesSlice';

export default function Booking() {
 const dispatcher = useDispatch()
 const  [date,setDate]= useState()
 const [source ,setSource] = useState()
 const [destination , setDestination] = useState() 
 const buses = useSelector(state=>state.buses)
async function searchForBuses(){
  try{
    const response = await dispatcher(getBuses({source,destination}));
    if(response.payload){
      await dispatcher(updateAvailableBuses({buses:response.payload}))
      await dispatcher(updateSrcDest({source,destination}))
    }
    if(!buses) alert('no buses found')      
}
    catch(e){
      status == 'error'
    }
}

return(
<div>
  <h1 className="font-sans text-xl font-semibold tracking-tight text-indigo-950  p-3">
    Booking
  </h1>
  <div className='flex flex-row'>
  <div className='w-9/12	'>
  <div className="flex flex-row w-8/12 ">
    <div className="w-5/6">
      <form className="flex items-start flex-col justify-start space-y-6 w-120">
        <motion.input   
          whileTap={{ scale: 0.96 }}
          onChange={(event)=>{setSource(event.target.value)}}  
          className="bg-white border-indigo-200 rounded shadow tracking-tighter pb-2 pl-3 pr-3 pt-2 text-slate-950	 w-full"/>
        <motion.input  
          whileTap={{ scale: 0.96 }}
          onChange={(event)=>{setDestination(event.target.value)}}  
          className="bg-white border-indigo-200 rounded shadow tracking-tighter pb-2 pl-3 pr-3 pt-2 text-slate-950	w-full"/>
      </form>
      </div>
    <div className='flex flex-col justify-evenly'>
    <motion.div 
    onClick={searchForBuses}
      whileHover={{ scale: 1.2 }}
      whileTap={{ scale: 0.9 }}
      transition={{ type: "spring", stiffness: 200, damping: 17 }}>
    <MdOutlineSearch size={30} />
    </motion.div>
    <motion.div
      whileHover={{ scale: 1.2 }}
      whileTap={{ scale: 0.9 }}
      transition={{ type: "spring", stiffness: 200, damping: 17 }}>
    <MdSwapVerticalCircle size={30}/>
    </motion.div>
    </div>
    </div>

    <div className='w-8/12'>

      <DatePicker
        getSelectedDay={setDate}
        labelFormat={"MMMM"}
        color={"#374e8c"}
        />
    </div>
    <div 
 className="flex flex-row justify-around w-8/12  bg-white shadow h-11 py-2 ">
<div className='w-1/4 text-center pb-2 pl-0 pr-0 pt-0  text-black text-base font-medium'>
  OPERATOR
</div>

<div className='w-1/4 text-center pb-2 pl-0 pr-0 pt-0  text-black text-base font-medium'>
TYPE
</div>
  
<div className='w-1/4 text-center pb-2 pl-0 pr-0 pt-0  text-black text-base font-medium'>
ARRIVAL
</div>
  
<div className='w-1/4 text-center pb-2 pl-0 pr-0 pt-0  text-black text-base font-medium'>
  SEATS
</div>
<div className='w-1/4 text-center pb-2 pl-0 pr-0 pt-0  text-black text-base font-medium'> 
  PRICE
</div>
</div>
  <AnimatePresence>
  {buses.availbleBuses && buses.availbleBuses.map((e,i)=>(
         <BusItem key={i}
         date = {date}
         id={e._id}
         operatorName={e.operatorName}
         busType={e.busType}
         arrivalTime={e.arrivalTime}
         totalSeats={e.totalSeats}
         price={e.price} />
         )
         )}
 </AnimatePresence>
</div>
<div className='flex flex-row justify-end'>
<div className='flex flex-col justify-around overflow-y-scroll h-10/12'>
{/* <Banner />
  <Banner />
  <Banner />
  <Banner /> */}
</div>
</div>
</div>
</div>

  )
}