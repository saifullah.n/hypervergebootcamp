import  {useState} from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { registerUser, updateUser  ,setUserSession } from '../redux/UserStore';
import { useNavigate } from 'react-router-dom';
import ReactLoading from 'react-loading'
function Register() {
  const dispatcher = useDispatch()
  const [email, setEmail] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const history = useNavigate();
  const [password, setPassword] = useState("");
  const [msg, setMsg] = useState("");
  const status = useSelector((state)=>(state.user.loading))
  const Auth = async(e)=>{
    e.preventDefault()
    if(! password.match( new RegExp(/^[a-zA-Z0-9]{3,30}$/))) return  setMsg('password must contain at least a digit , Upper/lowercase')
    try{
    const response = await dispatcher(registerUser({email,password,firstName,lastName ,phoneNumber}))
    console.log(response.payload);
    if(response.payload){ 
      dispatcher(updateUser({ token:response.payload.accessToken,
                              refreshToken : response.payload.refreshToken ,
                              email ,
                              name: `${firstName}  ${lastName}`
                            }))  
      dispatcher(setUserSession())   
      history('/booking')
  } else setMsg("Something went wrong")
}
    catch(e){
      status == 'error'
    }
  }
  
    if( !status || status == 'idle')
          return (<div className="flex min-h-full flex-1 flex-col justify-center px-6 py-12 lg:px-8">
        <div className="sm:mx-auto sm:w-full sm:max-w-sm">
          <h2 className="mt-10 text-center text-2xl font-bold leading-9 tracking-tight text-gray-900">
            Register your account
          </h2>
        </div>

        <div className="mt-10 sm:mx-auto sm:w-full sm:max-w-sm">
          <form className="space-y-6" onSubmit={Auth}>
            <div>
              <label htmlFor="email" className="block text-sm font-medium leading-6 text-gray-900">
                Email address
              </label>
              <div className="mt-2">
                <input
                  id="email"
                  name="email"
                  type="email"
                  autoComplete="email"
                  required
                  className="p-4 block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                  onChange={e=>(setEmail(e.target.value))}
                />
              </div>
            </div>
            <div>
              <label htmlFor="firstName" className="block text-sm font-medium leading-6 text-gray-900">
                First Name
              </label>
              <div className="mt-2">
                <input
                  id="firstName"
                  name="firstName"
                  type="text"
                  required
                  className="p-4 block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                  onChange={e=>(setFirstName(e.target.value))}
                />
              </div>
            </div>

            <div>
              <label htmlFor="lastName" className="block text-sm font-medium leading-6 text-gray-900">
                Last Name
              </label>
              <div className="mt-2">
                <input
                  id="lastName"
                  name="lastName"
                  type="text"
                  required
                  className="p-4 block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                  onChange={e=>(setLastName(e.target.value))}
                />
              </div>
            </div>

            <div>
              <label htmlFor="phoneNumber" className="block text-sm font-medium leading-6 text-gray-900">
                phone Number
              </label>
              <div className="mt-2">
                <input
                  id="phoneNumber"
                  name="phoneNumber"
                  type="number"
                  required
                  className="p-4 block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                  onChange={e=>(setPhoneNumber(e.target.value))}
                />
              </div>
            </div>

            <div>
              <div className="flex items-center justify-between">
                <label htmlFor="password" className="block text-sm font-medium leading-6 text-gray-900">
                  Password
                </label>
              </div>
              <div className="mt-2">
                <input
                  id="password"
                  name="password"
                  type="password"
                  autoComplete="current-password"
                  required
                  className="p-4block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                  onChange={e=>(setPassword(e.target.value))}
                />
              </div>
            </div>
            <div className="text-sm">
                  <p  className="font-italic text-red-600 hover:text-red-500">
                    {msg}
                  </p>
                </div>

            <div>
              <button
                type="submit"
                className="flex w-full justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"              >
                Register
              </button>
            </div>
          </form>

          <p className="mt-10 text-center text-sm text-gray-500">
            <a href='/login' className="font-semibold leading-6 text-indigo-600 hover:text-indigo-500">
            Aldready have an account
            </a>
          </p>
        </div>
      </div>)
    if (status == 'loading') 
          return( <ReactLoading type='SpinningBubbles' color="#fff" />   )
    if(status == 'error')
          return(<></>) 
    
      

}

export default Register