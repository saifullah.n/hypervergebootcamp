import axios from "axios";
export const acessPrivateRoute  = axios.create({
	// Configuration
	baseURL: `http://localhost:8080/`,
	timeout: 8000,
	headers: {
		Accept: 'application/json',
		'authorization': `Bearer ${JSON.parse(sessionStorage.getItem('user'))?.token}`
	},
});