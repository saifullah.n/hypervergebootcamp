/** @type {import('tailwindcss').Config} */

export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",

  ],
  plugins: [],
}

// daisyui: {
//   themes: [
//     {
//       mytheme: {
      
// "primary": "#60a5fa",
      
// "secondary": "#0369a1",
      
// "accent": "#e0f2fe",
      
// "neutral": "#f3f4f6",
      
// "base-100": "#e0f2fe",
      
// "info": "#2563eb",
      
// "success": "#22c55e",
      
// "warning": "#fcd34d",
      
// "error": "#f87171",
//       },
//     },
//   ],
// },
